package ca.uwo.cfmm.storageprovider;

import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.component.ComponentModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.storage.ldap.LDAPIdentityStoreRegistry;
import org.keycloak.storage.ldap.LDAPStorageProvider;
import org.keycloak.storage.ldap.LDAPStorageProviderFactory;
import org.keycloak.storage.ldap.idm.store.ldap.LDAPIdentityStore;
import org.keycloak.storage.ldap.mappers.LDAPConfigDecorator;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Properties;


public class PasswordUserStorageProviderFactory extends LDAPStorageProviderFactory {
   protected final Properties properties = new Properties();

   protected LDAPIdentityStoreRegistry ldapStoreRegistry2;

   static {
      configProperties.add(new ProviderConfigProperty("updateBaseDn", "updateBaseDn", "updateBaseDn",
         ProviderConfigProperty.STRING_TYPE, "ou=people,dc=example,dc=com"));
      configProperties.add(new ProviderConfigProperty("updateUrl", "updateUrl", "updateUrl",
         ProviderConfigProperty.STRING_TYPE, "ad.example.com"));
      configProperties.add(new ProviderConfigProperty("updatePrincipal", "updatePrincipal", "updatePrincipal",
         ProviderConfigProperty.STRING_TYPE, "cn=update,ou=people,dc=example,dc=com"));
      configProperties.add(new ProviderConfigProperty("updateCredential", "updateCredential", "updateCredential",
         ProviderConfigProperty.PASSWORD, "", true));
      configProperties.add(new ProviderConfigProperty("fail", "Fail after Update", "Fail validation after password update",
         ProviderConfigProperty.BOOLEAN_TYPE, false));
   }

   @Override
   public String getId() {
      return "cfmm-password-provider";
   }

   @Override
   public void init(Config.Scope config) {
      super.init(config);

      // ldapStoreRegistry is private, but we need access to it so get access via reflection
      try {
         Field privateIdentityStore = LDAPStorageProviderFactory.class.getDeclaredField("ldapStoreRegistry");
         privateIdentityStore.setAccessible(true);
         ldapStoreRegistry2 = (LDAPIdentityStoreRegistry) privateIdentityStore.get(this);
      } catch (IllegalAccessException | NoSuchFieldException e) {
         // If reflection fails, make a new object
         ldapStoreRegistry2 = new LDAPIdentityStoreRegistry();
      }
   }

   @Override
   public void close() {
      this.ldapStoreRegistry2 = null;
   }

   @Override
   public LDAPStorageProvider create(KeycloakSession session, ComponentModel model) {
      Map<ComponentModel, LDAPConfigDecorator> configDecorators = getLDAPConfigDecorators(session, model);

      properties.setProperty("updateBaseDn", model.getConfig().getFirst("updateBaseDn"));
      properties.setProperty("updateUrl", model.getConfig().getFirst("updateUrl"));
      properties.setProperty("updatePrincipal", model.getConfig().getFirst("updatePrincipal"));
      properties.setProperty("updateCredential", model.getConfig().getFirst("updateCredential"));
      properties.setProperty("fail", model.getConfig().getFirst("fail"));

      LDAPIdentityStore ldapIdentityStore = ldapStoreRegistry2.getLdapStore(session, model, configDecorators);
      return new PasswordUserStorageProvider(
         this,
         session,
         model,
         ldapIdentityStore,
         properties);
   }
}
