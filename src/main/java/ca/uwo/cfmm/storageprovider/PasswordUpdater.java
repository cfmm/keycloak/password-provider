package ca.uwo.cfmm.storageprovider;

import org.jboss.logging.Logger;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Hashtable;

public class PasswordUpdater {
   private static final Logger logger = Logger.getLogger(PasswordUserStorageProvider.class);

   public static boolean updatePasswordSource(String username, String password,
                                              String baseDn,
                                              String connectionUrl,
                                              String authPrincipal,
                                              String authCredential) {
      if (username == null || password == null || baseDn == null ||
          username.isEmpty() || password.isEmpty() || baseDn.isEmpty()) {
         logger.warn("Missing user information, unable to save password");
         return false;
      }

      final String userDn = "cn=" + username + "," + baseDn;

      if (authCredential == null || authPrincipal == null || connectionUrl == null ||
          authPrincipal.isEmpty() || authCredential.isEmpty() || connectionUrl.isEmpty()) {
         logger.warn("Missing credentials, unable to save password for " + userDn);
         return false;
      }

      LdapContext ldapContext = null;
      StartTlsResponse tlsResponse = null;

      try {
         Hashtable<String, String> ldapEnv = new Hashtable<>(11);
         ldapEnv.put("com.sun.jndi.ldap.connect.pool", "false");
         ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
         ldapEnv.put(Context.PROVIDER_URL, connectionUrl);
         ldapContext = new InitialLdapContext(ldapEnv, null);
         tlsResponse = (StartTlsResponse) ldapContext.extendedOperation(new StartTlsRequest());

         if (tlsResponse == null) {
            logger.warn("TLS failure. Unable to save password for " + userDn);
            return false;
         }

         tlsResponse.negotiate();

         ldapContext.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
         ldapContext.addToEnvironment(Context.SECURITY_PRINCIPAL, authPrincipal);
         ldapContext.addToEnvironment(Context.SECURITY_CREDENTIALS, authCredential);

         ModificationItem[] mods = new ModificationItem[2];

         // Create the encoded password
         String newQuotedPassword = "\"" + password + "\"";
         byte[] newUnicodePassword = newQuotedPassword.getBytes(StandardCharsets.UTF_16LE);

         // Update the password
         mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
            new BasicAttribute("UnicodePwd", newUnicodePassword));
         // Set NORMAL_ACCOUNT (0x0200) and DONT_EXPIRE_PASSWORD (0x10000)
         mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
            new BasicAttribute("userAccountControl", String.valueOf(0x10200)));

         ldapContext.modifyAttributes(userDn, mods);

      } catch (IOException | NamingException e) {
         logger.warn("Unable to save password for " + userDn);
         return false;
      } finally {
         if (tlsResponse != null) {
            try {
               tlsResponse.close();
            } catch (IOException e) {
               // Do nothing as there is no recovery possible
            }
         }
         if (ldapContext != null) {
            try {
               ldapContext.close();
            } catch (NamingException e) {
               // Do nothing as there is no recovery possible
            }
         }
      }
      return true;
   }
}
