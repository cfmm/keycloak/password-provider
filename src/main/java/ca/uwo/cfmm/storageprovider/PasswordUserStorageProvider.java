package ca.uwo.cfmm.storageprovider;


import org.keycloak.models.KeycloakSession;
import org.keycloak.component.ComponentModel;
import org.keycloak.storage.ldap.LDAPStorageProvider;
import org.keycloak.storage.ldap.idm.store.ldap.LDAPIdentityStore;
import org.keycloak.models.UserModel;
import org.keycloak.models.RealmModel;

import java.util.Properties;


public class PasswordUserStorageProvider extends LDAPStorageProvider {
   protected final Properties properties;

   public PasswordUserStorageProvider(PasswordUserStorageProviderFactory factory,
                                      KeycloakSession session,
                                      ComponentModel model,
                                      LDAPIdentityStore ldapIdentityStore,
                                      Properties ldapProperties) {
      super(factory, session, model, ldapIdentityStore);
      this.properties = ldapProperties;
   }

   @Override
   public boolean validPassword(RealmModel realm, UserModel user, String password) {
      boolean result = super.validPassword(realm, user, password);
      if (result) {
         // Update the password
         PasswordUpdater.updatePasswordSource(user.getUsername(), password,
            properties.getProperty("updateBaseDn"),
            properties.getProperty("updateUrl"),
            properties.getProperty("updatePrincipal"),
            properties.getProperty("updateCredential"));
      }
      return result && !Boolean.parseBoolean(properties.getProperty("fail"));
   }

}
