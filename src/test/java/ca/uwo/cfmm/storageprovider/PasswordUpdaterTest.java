package ca.uwo.cfmm.storageprovider;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;


public class PasswordUpdaterTest {
   final private Properties props = new Properties();


   @Test
   void passwordUpdateTest() {
      InputStream is = ClassLoader.getSystemResourceAsStream("unittest.properties");
      try {
         props.load(is);
      }
      catch (IOException e) {
         // Do nothing, defaults will be used
      }
      String password = props.getProperty("password", "password");
      final String username = props.getProperty("username", "someuser");
      final String baseDn = props.getProperty("baseDn", "OU=People,DC=example,DC=ca");
      final String dn = "cn=" + username + "," + baseDn;
      final String connectionUrl = props.getProperty("connectionUrl", "ldap://ad.example.com");

      if (checkbind(dn, password, connectionUrl)) {
         password += "new";
      }

      assertTrue(PasswordUpdater.updatePasswordSource(
         username,
         password,
         baseDn,
         connectionUrl,
         props.getProperty("authPrincipal", "cn=authuser,ou=people,dc=example,dc=ca"),
         props.getProperty("authCredential", "secret")
         ));

      assertTrue(checkbind(dn, password, connectionUrl));
   }

   boolean checkbind(String dn, String password, String connectionUrl) {
      LdapContext ldapContext = null;
      StartTlsResponse tlsResponse = null;

      try {
         Hashtable<String, String> ldapEnv = new Hashtable<>(10);
         ldapEnv.put("com.sun.jndi.ldap.connect.pool", "false");
         ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
         ldapEnv.put(Context.PROVIDER_URL, connectionUrl);
         ldapContext = new InitialLdapContext(ldapEnv, null);
         tlsResponse = (StartTlsResponse) ldapContext.extendedOperation(new StartTlsRequest());

         if (tlsResponse == null) {
            return false;
         }

         tlsResponse.negotiate();

         ldapContext.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
         ldapContext.addToEnvironment(Context.SECURITY_PRINCIPAL, dn);
         ldapContext.addToEnvironment(Context.SECURITY_CREDENTIALS, password);

         ldapContext.reconnect(null);
      } catch (IOException | NamingException e) {
         return false;
      } finally {
         if (tlsResponse != null) {
            try {
               tlsResponse.close();
            } catch (IOException e) {
               // Do nothing as there is no recovery possible
            }
         }
         if (ldapContext != null) {
            try {
               ldapContext.close();
            } catch (NamingException e) {
               // Do nothing as there is no recovery possible
            }
         }
      }
      return true;
   }
}
